object Form7: TForm7
  Left = 0
  Top = 0
  Caption = 'Cadastro'
  ClientHeight = 393
  ClientWidth = 393
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 112
    Top = 8
    Width = 160
    Height = 42
    Caption = 'Cadastro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -27
    Font.Name = 'Trajan Pro 3'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
  end
  object TLabel
    Left = 30
    Top = 104
    Width = 115
    Height = 18
    Caption = 'Nome Completo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 24
    Top = 181
    Width = 3
    Height = 18
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 30
    Top = 181
    Width = 129
    Height = 18
    Caption = 'Data da Consulta:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 202
    Top = 104
    Width = 43
    Height = 18
    Caption = 'Email:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 30
    Top = 249
    Width = 71
    Height = 18
    Caption = 'Telefone:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 202
    Top = 249
    Width = 65
    Height = 18
    Caption = 'Celular:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clMaroon
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 24
    Top = 128
    Width = 121
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object MaskEdit1: TMaskEdit
    Left = 28
    Top = 273
    Width = 111
    Height = 25
    EditMask = '!\(999\)000-0000;1;_'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = []
    MaxLength = 13
    ParentFont = False
    TabOrder = 1
    Text = '(   )   -    '
  end
  object MaskEdit2: TMaskEdit
    Left = 24
    Top = 205
    Width = 120
    Height = 25
    EditMask = '!99/99/00;1;_'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = []
    MaxLength = 8
    ParentFont = False
    TabOrder = 2
    Text = '  /  /  '
  end
  object Edit3: TEdit
    Left = 202
    Top = 128
    Width = 121
    Height = 25
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object MaskEdit3: TMaskEdit
    Left = 202
    Top = 273
    Width = 119
    Height = 25
    EditMask = '!\(99\)0000-0000;1;_'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Trajan Pro 3'
    Font.Style = []
    MaxLength = 13
    ParentFont = False
    TabOrder = 4
    Text = '(  )    -    '
  end
end
